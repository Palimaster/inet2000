var transKey = [
    ['.-',      'a'],
    ['-...',    'b'],
    ['-.-.',    'c'],
    ['-..',     'd'],
    ['.',       'e'],
    ['..-.',    'f'],
    ['--.',     'g'],
    ['....',    'h'],
    ['..',      'i'],
    ['.---',    'j'],
    ['-.-',     'k'],
    ['.-..',    'l'],
    ['--',      'm'],
    ['-.',      'n'],
    ['---',     'o'],
    ['.--.',    'p'],
    ['--.-',    'q'],
    ['.-.',     'r'],
    ['...',     's'],
    ['-',       't'],
    ['..-',     'u'],
    ['...-',    'v'],
    ['.--',     'w'],
    ['-..-',    'x'],
    ['-.--',    'y'],
    ['--..',    'z'],
    ['.----',   '1'],
    ['..---',   '2'],
    ['...--',   '3'],
    ['....-',   '4'],
    ['.....',   '5'],
    ['-....',   '6'],
    ['--...',   '7'],
    ['---..',   '8'],
    ['----.',   '9'],
    ['-----',   '0'],
];

var toTranslate;
var outputArray = []; //holds value to be displayed on-screen after translation
//The following runs the translate function every 500 miliseconds, or half a second.
window.setInterval(function(){
  translate();
}, 1000);




function translate() {
    outputArray=[];
    toTranslate = document.getElementById('userInputField').value;
    
                //translate to alphabet
    if (toTranslate.charAt(0) === '.' || toTranslate.charAt(0) === '-') { 
        var translateArray = toTranslate.split("|", toTranslate.length);
        var x=0; //determines what side of the key to look at
        for (var i = 0; i < translateArray.length; i++) {
            if (translateArray[i] == " "){
                outputArray+= " ";
            }
            for (var y=0; y<transKey.length; y++){
               if (translateArray[i] == transKey[y][x]){
                    outputArray+=transKey[y][x+1];
                }
            }
        }
    }
    
                 //translate to morse
    else {             
        var translateArray = toTranslate.split("", toTranslate.length);
        var x=1; //determines what side of the key to look at
        for (var i = 0; i < translateArray.length; i++) {
            if (translateArray[i] == " "){
                outputArray+= "  |";
            }
            for (var y=0; y<transKey.length; y++){
                if (translateArray[i] == transKey[y][x]){
                    outputArray+=transKey[y][x-1]+'|';
                }
            }
        }
    }
    $("#output").text(outputArray.toString());
}
var app = new Vue ({ //new Vue instance
    
    el: '#root', //vue bound to "root" div
    
    data: {
        
    }
    
});
// it should be stated, that these components rely on pre-defined stilesheet stuff from "bulma"
// to show them, <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.2.3/css/bulma.css> would need to be added"
// however, remember that the first one doesn't actually work properly.
//when creating a component, if you want to pass something into it, say to give it a title, it needs to be explicitly allowed
Vue.component('message',{
    template: `
    
    <article class=message">
        <div class="message-header">
            {{title}}
        </div>
        
        <div class="message-body">
            {{body}}
        </div>
    </article>
    `
});
/*
as is, the above won't work, that's because we didn't tell it that the "title" or "body" props aren't allowed to be passed in, so it just kinda, doesn't do anything.
to fix that, we just need to add the acceptable props like the following:
*/
Vue.component('message',{
    
    props: ['title','body'],
    
    template: `
    
    <article class=message">
        <div class="message-header">
            {{title}}
        </div>
        
        <div class="message-body">
            {{body}}
        </div>
    </article>
    `
});

//these could be further modified, to say, have a button that closes them down we could simply use a method, which when a button is clicked, sets the visibility to false,
//which will hide the item. This could be usefull for say, an announcement section, that users can close after they're done with it, and then it wouldn't take up screen space.
//remember to decide: "Do I need to create a whole method for this" or "can I do this in-line with very little"
//there's no reason to create a whole method to do something that can be done with saying "setVisible" in a @click: in-line method. but it's important to not make a large mess
//by trying to include too much in-line.
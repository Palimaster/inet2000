Vue.component('task',{ //this is used to make a new component, like an html component. so instead of using <div>, we could use <task>
   
   template: '<li><slot></slot></li>' // a template is helpful, as it sets a base of what the item should behave like, for example, a <li> or list item.
                // to make it so we have a dynamic entry in the new item, we use <slot> tags, which essentially leaves an open space that will actually just pass
                //whatever is entered between the <task> tags on the html page.
    //you can also give it its own set of pre-defined data or something, by including a data object, and returning an object, like the following:
    /*
    data() { //the parenthesis here make data equal to a function, instead of data: {}, which makes data equal to an object.
                // inside a component, data cannot be equal to an object.
        /stuff
        return{
            //stuff to return such as:
            message: 'Foobar'
        };
    }
    
    */
});

var app = new Vue ({ //new Vue instance
       
    el: '#root', //vue bound to "root" div
});

/*
This is an example of a component with a hard-coded value. if we use this, any <task> item, will always contain "Foobar", regardless of what we type in between the tags on the html page.
Vue.component('task',{ 
   
   template: '<li>Foobar</li>' // a template is helpful, as it sets a base of what the item should behave like, for example, a <li> or list item.
    
});
*/
Vue.component('task-list',{ 
   
   template: '<div><task v-for="task in tasks">{{task.task}}</task></div>', //since we have an array of tasks, and a <task> element is already defined,
                                    //we can have the template be that created <task> element we already defined.
                                    //we can also set the v-for here, so it will go through all the pre-defined items in the tasks array and
                                    //generate all the items needed.
                                    //however, an important thing to note, is that this won't work unless all of the task elements to be created
                                    //are wrapped in an element that will only have 1 instance. to clarify, since we are essentially making as
                                    //many task elements as are in the 'tasks' array, we get an error. however, if we wrap it in a, say, <div>
                                    //as shown, it will be fine.
                                    //also, ES6 standards allow the template to be layed out as you would do on an html page,
                                    //so:
                                    /*
                                    `
                                    <div>
                                        <task v-for="task in tasks">
                                            {{task.task}}
                                        </task>
                                    </div>
                                    `
                                    */
                                    //could be used instead of declaring it all on one line.
                                    //however, it needs to be surrounded by "back tics" (the backwards facing single quote usually on the tilde(~) key)
   
   data() {
       
       return{
            tasks:[
                
                { task: 'Go to store',       completed: true},
                { task: 'Finish homework',   completed: false},
                { task: 'Make dinner',       completed: true},
                { task: 'Check e-mail',      completed: false},
                { task: 'Organize stuff',    completed: false},
                { task: 'Pet noodles',       completed: true}
            ]
       }
   }

});


Vue.component('task',{ 
   
   template: '<li><slot></slot></li>'

});


var app = new Vue ({ //new Vue instance
       
    el: '#root', //vue bound to "root" div
});

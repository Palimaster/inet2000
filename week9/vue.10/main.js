Vue.component('modal',{
   
   template: `
   <div class="modal is-active">
                <div class="modal-background"></div>
                <div class="modal-content">
                    <div class="box">
                        
                            <slot></slot>
                        
                    </div>
                </div>
                <button class="modal-close" @click="$emit('close')"></button>
            </div>
            `
}); //above on the button, we have a custom announcement event, that is based on the click event
    //it says, when clicked, emit/generate an event that says "close", essentially having the modal announce it is closed.
    
    //the slot tags allow us to have various things instead of a hard-coded paragraph. sometimes a hard-coded paragraph is all that's needed, but usually it's better
    //to allow it to be updated (or kept the same) somewhere else, as digging through the code to change it is generally not preferred.



var app = new Vue ({ //new Vue instance
            
            el: '#root', //vue bound to "root" div
            
            data: {
                
                showModal: false,
                
            }
            
        });
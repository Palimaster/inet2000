//vue 2 has a "simple event emitter", that allows events to go between sibling components easily. This allows any component, or even a new vue instance, to listen to or emit events.

//this new class event instance will do stuff on any event, which the first one will happen immeditaly
window.Event = new class{
    //when the constructor is run, it will create a new Vue instance, which allows us to make a custom api with stuff
    constructor() {
        this.vue = new Vue();
    }
    
    //this says to fire (activate) an event (named event), and data is not required.
    fire(event, data =null){
        this.vue.$emit(event,data);
    }
    //this says it will listen for an event(named event), and then trigger a callback function (named callback). it is essentially a wrapper, so we type listen(event, callback) instead of the this.vue.$on
    listen(event, callback){
        this.vue.$on(event, callback);
    }
};

//the blur event is when you click off of something (basically).
Vue.component('coupon',{
    template: '<input placeholder="Enter your coupon code" @blur="onCouponApplied">',
    
    methods: {
        
        onCouponApplied(){
            //Event.$emit('applied',); //this is an event emitter. using $on would be a listener
            Event.fire('applied'); //this uses the created wrappers, which tend to be cleaner to look at, but don't actually affect useage.
        }
    }
});

//it's important to declare components before the root Vue instance, as otherwise errors will get thrown, as it is referencing something that essentially doesn't exist yet.
var app = new Vue ({ //new Vue instance
    
    el: '#root', //vue bound to "root" div
    
    data: {
        
        couponApplied: false
    },
    
    /* // this is an example of a kind of low-tech listener via functions, which really are limited to parent-child interactions. and can make communicating between siblings difficult.
    methods: {
        onCouponApplied() {
            this.couponApplied = true;
        }
    }
    */
    
    //this is the better listener, that will be there on creation since it's using a created device.
    //this says for Event "$on", (which is a listener) 'applied' (so it's looking for the 'applied' event emmision), respond, but instead of a function, just push it to an alert.
    created() {
       // Event.$on('applied', () => alert('Handling it!'));//these are similar to the event emitters above, just using listeners instead.
       Event.listen('applied', () => alert('Handling it!'));
    }
    
});


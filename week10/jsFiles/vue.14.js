//by default, if you have multiple <slot> tags in a component, they will ALL display the text entered in the html <modal> tags.
//a way to work around this, is simply naming the slots.

//this component is based on a template from bulma.

//we have changed out the hard-coded text in the modal title for a slot named header, the body text for the default slot, which makes any un-defined data go into the default slot
// we can leave text inside the default slot, or other slots for that matter, and then they can be overridden by the user when they use the modal tags in the html document (DOM).
//the default contents of a slot tag can be other things besides text, in this case, we have a generic button that says ok
Vue.component("modal", {
    template: `
        <div class="modal is-active">
            <div class="modal-background"></div>
            <div class="modal-card">
                <header class="modal-card-head">
                    <p class="modal-card-title">
                        <slot name="header"></slot>
                    </p>
                    <button class="delete"></button>
                </header>
                <section class="modal-card-body">
                    <slot>
                        Default content when user defined content is not entered
                    </slot>
                </section>
                <footer class="modal-card-foot">
                    <slot name="footer">
                        <a class="button is-primary">OK</a>
                        
                    </slot>
                </footer>
            </div>
        </div>
    `
});
    





var app = new Vue ({ //new Vue instance
            
            el: '#root', //vue bound to "root" div
            
            data: {
                
                
            }
            
        });
Vue.component('tabs', {
    
    //the first part of the template is based off of a template from the bulma library.
    //the second part is custom defined, and is used to keep any other stuff added in between the "tabs" tag, via the <slot> tag.
    //remember, when making templates, there can't be sibling tags at the top level, anything defined in a single tmeplate must be contained withing a single parent tag, which is what the outermost div is for.
    //the origional list items have been cut out, as since we have details and names defined, we can simply echo out the name field, as it is what the tab should be named anyways.
    //the <a href> tag is important, as it is an achor that the tab is supposed to reference on click.
    //without the anchoring tag, spacing will be messed up, essentially there will be no spacing between items.
    
    // the class binding gives the tag an is-active class (which is used by bulma for formatting) if the tab.selected prop defined below is true.
    
    //binding to "tab.href" allows us to update the webpage url tag, so it would be possible to link to the page with a specific tab already selected.
    //it has to be defined, and so it is done so below in the 'tab' component.
   template: `
        <div>
            <div class="tabs">
                <ul>
                    <li v-for="tab in tabs" :class="{'is-active': tab.isActive}">
                        <a :href="tab.href" @click="selectTab(tab)">{{tab.name}}</a>
                    </li>
                </ul>
            </div>
            
            
            <div class="tabs-details">
                <slot></slot>
            </div>
        </div>
   `,
   
   //we have to be explicit of what data/components we are exposing, so we need to return an empty array called tabs so the 'created'
   //section below will have something to work with and won't throw an error.
   data() {
       return {tabs: []};
       
   },
   
   //this says, once this is template is created, create a "tabs" item that contains the same info as the child elements.
   //this will allow for easy targeting.
   created() {
       this.tabs = this.$children;
   },
   
   
   //this says: once the template is mounted (aka up and running), output what the children are to console.
   mounted() {
       console.log(this.$children);
   },
   
   methods: {
       /*
       //this method doesn't work, via a limitation in Vue. it will throw an error.
       //basically, changing a prop directly like this will not work, as the value will be changed back to the origional whenever the parent component needs to be re-rendered.
       //if a changeable property is needed, us a 'computed' property or a 'data' property, like the one created in the 'tab' component for this reason.
       selectTab(){
           this.tabs.forEach(tab => {
               tab.selected = (tab.name == selected.name);
           });
           
       },*/
       
       selectTab(selectedTab){
           this.tabs.forEach(tab => {
               tab.isActive = (tab.name == selectedTab.name);
           });
       },
       
   }
    
});

//while we have most of the elements for tabs built above, we haven't defined the sub-component, each individual <tab> tag, so that is done below.
//remember, any props that will be accepted on custom elements must be explicitly allowed.
//in this case, we are saying than the "name" tag MUST be filled out.
//v-show is used to display/hide content that is/isn't related to the active tab.
Vue.component('tab',{
    template: `
        <div v-show="isActive"><slot></slot></div>
    `,
    
    // the name tag will be required
    //we are using a "selected" prop to decide which tab should be formatted to look like it is selected
    props: {
        name: {requred: true},
        selected: {default:false},
    },
    
    //the order of these is important, basically, have an isActive property that will be used, then change it to the selected property, which will
    //change the status of each tab using the working method property above
    data(){
        return{
            isActive:false
        };
    },
    
    //this computed property is to decide what the link to the tab.href in each tab ends up showing.
    //it could have been entered as a method, but it is probably better to implement it as a computed property, since it is computed, and not so much a method.
    computed:{
        href(){
            //this says, return a tag that is the current name, as lowercase, with any spaces replaced (/ /g) as dashes ('-')
            return '#'+ this.name.toLowerCase().replace(/ /g, '-');
        }
    },
    
    mounted(){
        this.isActive = this.selected;
    }
    
});


new Vue ({ //new Vue instance
    
    el: '#root', //vue bound to "root" div
    
    
    
});